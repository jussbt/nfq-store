package lt.nfq.academy.store.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.nfq.academy.store.constants.ErrorMessages;
import lt.nfq.academy.store.model.Movie;
import lt.nfq.academy.store.repositories.MovieRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class MovieService {
    final MovieRepo movieRepo;

    @Autowired
    public MovieService(MovieRepo movieRepo) {
        this.movieRepo = movieRepo;
    }

    @Transactional
    public void saveMoviesArrayIntoDatabase(Movie[] movies) {
        List<Movie> movieList = Arrays.asList(movies);
        movieRepo.saveAll(movieList);
    }

    @Transactional
    public List<Movie> getAllMovies() {
        return movieRepo.findAll();
    }

    @Transactional
    public void saveMultiple(String json) {
        try {
            Movie[] movies = new ObjectMapper().readValue(json, Movie[].class);
            saveMoviesArrayIntoDatabase(movies);
        } catch (IOException e) {
            throw new RuntimeException(ErrorMessages.BAD_FORMAT_TEXT);
        }
    }
}
