package lt.nfq.academy.store.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.nfq.academy.store.constants.ErrorMessages;
import lt.nfq.academy.store.model.MoviePurchase;
import lt.nfq.academy.store.repositories.MoviePurchaseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class PurchaseService {
    private final MoviePurchaseRepo moviePurchaseRepo;

    @Autowired
    public PurchaseService(MoviePurchaseRepo moviePurchaseRepo) {
        this.moviePurchaseRepo = moviePurchaseRepo;
    }

    @Transactional
    public void saveMoviePurchaseIntoDatabase(MoviePurchase[] moviePurchases) {
        moviePurchaseRepo.saveAll(Arrays.asList(moviePurchases));
    }

    @Transactional
    public List<Map> getAllPurchasesWithPrices() {
        return moviePurchaseRepo.getAllPurchases();
    }

    @Transactional
    public void registerMultipleMoviePurchases(String json) {
        try {
            MoviePurchase[] movies = new ObjectMapper().readValue(json, MoviePurchase[].class);
            saveMoviePurchaseIntoDatabase(movies);
        } catch (IOException e) {
            throw new RuntimeException(ErrorMessages.BAD_FORMAT_TEXT);
        }
    }
}
