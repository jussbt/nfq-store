package lt.nfq.academy.store.controller;

import lt.nfq.academy.store.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/purchases")
@CrossOrigin(origins = "*")
public class PurchaseController {
    private final PurchaseService moviePurchaseService;

    @Autowired
    public PurchaseController(PurchaseService moviePurchaseService) {
        this.moviePurchaseService = moviePurchaseService;
    }

    @GetMapping(value = "/movies")
    public List<Map> returnAllMoviePurchases() {
        return moviePurchaseService.getAllPurchasesWithPrices();
    }

    @PostMapping(value = "/multiple")
    public void registerMultipleMoviePurchases(@RequestBody String json) {
        moviePurchaseService.registerMultipleMoviePurchases(json);
    }
}
