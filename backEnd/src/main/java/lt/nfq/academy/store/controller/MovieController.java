package lt.nfq.academy.store.controller;

import lt.nfq.academy.store.model.Movie;
import lt.nfq.academy.store.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/movies")
@CrossOrigin(origins = "*")

public class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public List<Movie> getAll() {
        return movieService.getAllMovies();
    }

    @PostMapping("/multiple")
    public void saveMultipleMovies(@RequestBody String json) {
        movieService.saveMultiple(json);
    }

}
