package lt.nfq.academy.store.repositories;

import lt.nfq.academy.store.model.MoviePurchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MoviePurchaseRepo extends JpaRepository<MoviePurchase, Long> {
    @Query("select new map(mp.id as id, mp.buyerName as buyer, m.name as movieName, m.price*mp.quantity as totalPrice ) " +
            "from MoviePurchase mp left join Movie m on m.id=mp.movieId ")
    List<Map> getAllPurchases();
}
