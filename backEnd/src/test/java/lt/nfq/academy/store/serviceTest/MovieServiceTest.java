package lt.nfq.academy.store.serviceTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.nfq.academy.store.model.Movie;
import lt.nfq.academy.store.repositories.MovieRepo;
import lt.nfq.academy.store.services.MovieService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceTest {
    @Mock
    MovieRepo movieRepo;
    @InjectMocks
    MovieService movieService;

    @Test
    public void getAllMoviesSuccess() {
        when(movieService.getAllMovies()).thenReturn(Collections.emptyList());
        movieService.getAllMovies();
        verify(movieRepo, times(1)).findAll();
    }

    @Test
    public void tryToSaveTwoMovies() throws JsonProcessingException {
        movieService.saveMultiple(returnTwoMovies());
        verify(movieRepo, times(1)).saveAll(anyList());
    }

    private String returnTwoMovies() throws JsonProcessingException {
        Movie movie1 = Movie.builder().genre("Romance").name("Lovely MOVIE").price(10.5).quantity(10L).build();
        Movie movie2 = Movie.builder().genre("Comedy").name("FUNNY MOVIE").price(15.0).quantity(1L).build();
        return new ObjectMapper().writeValueAsString(Arrays.asList(movie1, movie2));
    }

}
