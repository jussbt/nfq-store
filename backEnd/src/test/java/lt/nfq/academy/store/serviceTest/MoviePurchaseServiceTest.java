package lt.nfq.academy.store.serviceTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.nfq.academy.store.model.MoviePurchase;
import lt.nfq.academy.store.repositories.MoviePurchaseRepo;
import lt.nfq.academy.store.services.PurchaseService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MoviePurchaseServiceTest {
    @Mock
    MoviePurchaseRepo moviePurchaseRepo;
    @InjectMocks
    PurchaseService purchaseService;

    @Test
    public void getAllPurchases() {
        when(moviePurchaseRepo.getAllPurchases()).thenReturn(Collections.EMPTY_LIST);
        Collection answer = purchaseService.getAllPurchasesWithPrices();
        Assert.assertTrue(answer.isEmpty());
        verify(moviePurchaseRepo, times(1)).getAllPurchases();
    }

    @Test
    public void tryToSaveTwoPurchasesFromJson() throws JsonProcessingException {
        purchaseService.registerMultipleMoviePurchases(serializeTwoPurchases());
        verify(moviePurchaseRepo, times(1)).saveAll(anyList());

    }

    private String serializeTwoPurchases() throws JsonProcessingException {
        MoviePurchase moviePurchase1 = MoviePurchase.builder().buyerName("Peter Pen").movieId(1L).quantity(10L).build();
        MoviePurchase moviePurchase2 = MoviePurchase.builder().buyerName("John snow").movieId(2L).quantity(20L).build();
        return new ObjectMapper().writeValueAsString(Arrays.asList(moviePurchase1, moviePurchase2));

    }
}
