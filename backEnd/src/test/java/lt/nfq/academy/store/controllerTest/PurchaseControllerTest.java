package lt.nfq.academy.store.controllerTest;

import lt.nfq.academy.store.controller.PurchaseController;
import lt.nfq.academy.store.services.PurchaseService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PurchaseController.class)
public class PurchaseControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    PurchaseService purchaseService;

    @Test
    public void getAllPurchases() throws Exception {
        given(purchaseService.getAllPurchasesWithPrices()).willReturn(Collections.emptyList());
        mockMvc.perform(get("/api/purchases/movies"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void saveMultiplePurchases() throws Exception {
        mockMvc.perform(post("/api/purchases/multiple")
                .contentType(MediaType.APPLICATION_JSON).content(new JSONObject().toString()))
                .andExpect(status().isOk());
        verify(purchaseService, times(1)).registerMultipleMoviePurchases(anyString());
    }
}
