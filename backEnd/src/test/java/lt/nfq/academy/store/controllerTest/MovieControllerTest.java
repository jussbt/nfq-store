package lt.nfq.academy.store.controllerTest;

import lt.nfq.academy.store.controller.MovieController;
import lt.nfq.academy.store.services.MovieService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MovieController.class)
public class MovieControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    MovieService movieService;

    @Test
    public void getAllMoviesTest() throws Exception {
        given(movieService.getAllMovies()).willReturn(Collections.emptyList());
        mockMvc.perform(get("/api/movies"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void saveMultipleMovies() throws Exception {
        mockMvc.perform(post("/api/movies/multiple")
                .contentType(MediaType.APPLICATION_JSON).content(new JSONObject().toString()))
                .andExpect(status().isOk());
        verify(movieService, times(1)).saveMultiple(anyString());
    }
}
