package lt.nfq.academy.store.integration;

import lt.nfq.academy.store.controller.PurchaseController;
import lt.nfq.academy.store.model.MoviePurchase;
import lt.nfq.academy.store.repositories.MoviePurchaseRepo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseIT {
    @Autowired
    MoviePurchaseRepo moviePurchaseRepo;
    @Autowired
    PurchaseController purchaseController;

    @Before
    public void saveIntoDB() {
        purchaseController.registerMultipleMoviePurchases("[{\"id\":null,\"buyerName\":\"Peter Pen\",\"movieId\":1,\"quantity\":10},{\"id\":null,\"buyerName\":\"John snow\",\"movieId\":2,\"quantity\":20}]");
    }

    @Test
    public void moviePurchasesAreNotEmpty() {
        List<MoviePurchase> moviePurchases = moviePurchaseRepo.findAll();
        Assert.assertFalse(moviePurchases.isEmpty());
    }
}
