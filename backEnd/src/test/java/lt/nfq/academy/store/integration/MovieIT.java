package lt.nfq.academy.store.integration;

import lt.nfq.academy.store.controller.MovieController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MovieIT {
    @Autowired
    MovieController movieController;

    @Before
    public void prepareDB() {
        movieController.saveMultipleMovies("[{\"id\":null,\"name\":\"Lovely MOVIE\",\"genre\":\"Romance\",\"price\":10.5,\"quantity\":10},{\"id\":null,\"name\":\"FUNNY MOVIE\",\"genre\":\"Comedy\",\"price\":15.0,\"quantity\":1}]");

    }

    @Test
    public void moviesDatabaseIsNotEmpty() {
        Assert.assertFalse(movieController.getAll().isEmpty());
    }
}
