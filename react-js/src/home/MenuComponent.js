import React from 'react';
import FlexView from 'react-flexview';

const MenuComponent = props => {
    const redirectInto = value => {
        props.history.push(`/${value}`);
    };
    return (
        <FlexView column={true} >
            <FlexView className="ui menu" >
                <button  className="item active" onClick={() => redirectInto(``)}>
                <i className="home icon"></i>
                    Namai
                </button>
                <button className="item" onClick={() => redirectInto(`filmai`)}>
                <i className="film icon"></i>
                    Mūsų produktai
                </button>
                <button className="item" onClick={() => redirectInto(`pirkimai`)}>
                <i className="dollar sign icon"></i>
                    Užsakymai
                </button>               
            </FlexView>
                <FlexView  hAlignContent={'center'}>{props.content}</FlexView>          
        </FlexView>
    );
};
export default MenuComponent;

