import React from 'react';
import MenuComponent from './MenuComponent';
import greeting from './HomeGreetings';

class HomePageContainer extends React.Component {
   
    render() {
        return (
            <MenuComponent               
                content={greeting}
                history={this.props.history}
            />
        );
    }
}
export default HomePageContainer;
