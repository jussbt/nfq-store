import React from 'react';
import FlexView from 'react-flexview/lib';
import HomePageStyles from './HomePageStyles';
const frontEndBackEnd=(
    <FlexView className="ui cards" column={true}>
        <FlexView className="card" column={true}>
            <FlexView className="content" column={true}>
                <FlexView className="header" column={true}>
                     ReactJS
                </FlexView>
                <FlexView className="meta">
                Vienas populiariausių ir geriausių frameworkų kurti web aplikacijas.
                </FlexView>      
            </FlexView>    
        </FlexView>
        <FlexView className="card" column={true}>
            <FlexView className="content" column={true}>
                <FlexView className="header" column={true}>
                    Java 8 /Spring Boot 2.0
                </FlexView>
                <FlexView className="meta">
                Saugus , greitas ir paprastas būdas serveriukui užkurti.
                </FlexView>      
            </FlexView>   
    </FlexView>
</FlexView>
)
const taskList = (
    <FlexView className="ui middle aligned FlexViewided list" column={true} >
        <FlexView className="item" >           
           <i className="check circle icon" style={{ color: 'green' }} />
           <span style={HomePageStyles.taskExplanation}>Duomenų struktūrose (objektuose, masyvuose, localstorage ar pan.) turi būti bent po 50 prasmingų įrašų</span> 
        </FlexView>
        <FlexView className="item">           
            <i className="check circle icon" style={{ color: 'green' }} />
            <span style={HomePageStyles.taskExplanation}>Vartotojo sąsaja turi būti sugeneruojama iš turimų duomenų ir pagal juos atvaizduojama (rendered)</span>                
        </FlexView>
        <FlexView className="item" >
            <i className="check circle icon" style={{ color: 'green' }} />
            <span style={HomePageStyles.taskExplanation}>Konkrečių prekių/paslaugų ar užsakymų paieška turi veikti be puslapio perkrovimo (refresh)</span>     <br/>  
        </FlexView>
        <FlexView className="item">          
            <i className="check circle icon" style={{ color: 'green' }} />
            <span style={HomePageStyles.taskExplanation}>Paieškos rezultatas atvaizduojamas tame pačiame sąraše </span>     
        </FlexView>
    </FlexView>
);
const greeting = (
    <FlexView  column={true} hAlignContent={'center'}>       
        <FlexView  marginTop={30} style={HomePageStyles.taskExplanation}>
        {frontEndBackEnd}                    
        </FlexView>       
        <FlexView  marginTop={50} >
            {taskList}
        </FlexView>
    </FlexView>
);

export default greeting;
