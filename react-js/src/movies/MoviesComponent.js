import React from 'react';
import MenuComponent from '../home/MenuComponent';

const MoviesComponent = props => {
    return <MenuComponent history={props.history} content={props.content} />;
};
export default MoviesComponent;
