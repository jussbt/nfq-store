import React from 'react';
import { Pagination, List } from 'semantic-ui-react';
import FlexView from 'react-flexview/lib';
import MovieDetailsComponent from './MovieDetailsComponent';

class MovieList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activePage: 1,
            movies: this.props.movies
        };
    }
    setActivePage = (e, { activePage }) => {
        this.setState({
            activePage: activePage
        });
    };
    showMovies = () => {
        const start = (this.state.activePage - 1) * 10;
        const end = this.state.activePage * 10;
        return this.props.movies.slice(start, end);
    };
    filterMovies=(e)=>{
        this.setState({
            activePage:1
        });
        this.props.filterMovies(e);
    }

    render() {
        return (
            <FlexView column={true}>              
                <input
                    type="text"
                    placeholder="Filmų paieška pagal pavadinimą ir/arba žanrą"
                    onChange={e=>this.filterMovies(e)}
                />
                <FlexView className="ui celled list" row="true">
                    <List divided animated>
                        {this.showMovies().map(movie => {
                            return <MovieDetailsComponent key={movie.id} movie={movie} />;
                        })}
                    </List>
                </FlexView>
                <Pagination
                    activePage={this.state.activePage}
                    boundaryRange={1}
                    size="mini"
                    totalPages={this.props.movies.length / 10}
                    onPageChange={this.setActivePage}
                    siblingRange={2}
                />
            </FlexView>
        );
    }
}
export default MovieList;
