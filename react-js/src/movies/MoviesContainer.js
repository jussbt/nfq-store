import React from 'react';
import MoviesComponent from './MoviesComponent';
import axios from 'axios';
import { BASE_URL } from '../util/Constants';
import MovieList from './MovieList';
class MoviesContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [],
            searchValue: ''
        };
    }
    componentDidMount = () => {
        this.getAllMovies();
    };
    getAllMovies = () => {
        axios
            .get(BASE_URL + '/movies')
            .then(response => {
                this.setState({
                    movies: response.data
                });
            })
            .catch(error => {
                console.log(error);
            });
    };
    filterMovies = e => {
        this.setState({
            searchValue: e.target.value
        });
    };
    render() {
        return (
            <MoviesComponent
                history={this.props.history}
                movies={this.state.movies}
                content={
                    <MovieList
                        movies={this.state.movies.filter(
                            movie =>
                                movie.name.includes(this.state.searchValue) ||
                                movie.genre.includes(this.state.searchValue)
                        )}
                        filterMovies={this.filterMovies}
                    />
                }
            />
        );
    }
}
export default MoviesContainer;
