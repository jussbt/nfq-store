import React from 'react';
import FlexView from 'react-flexview/lib';

const MovieDetailsComponent = props => {
    return (
        <FlexView className="ui middle aligned divided list" style={styles.text} column={true} >
            <FlexView>Pavadinimas: {props.movie.name}</FlexView>
            <FlexView>Žanras: {props.movie.genre}</FlexView>
            <FlexView>Kaina: {props.movie.price}</FlexView>
        </FlexView>
    );
};
export default MovieDetailsComponent;
const styles = {
    text: {
        fontSize: 12,
        fontWeight: 'bold'
    }
};
