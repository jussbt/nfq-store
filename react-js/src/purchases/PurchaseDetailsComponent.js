import React from 'react';
import FlexView from 'react-flexview/lib';

const PurchaseDetailsComponent = props => {
    return (
        <div className="ui middle aligned divided list" style={styles.text}>
            <FlexView>Pirkėjas: {props.purchase.buyer}</FlexView>
            <FlexView>Filmo pavadinimas: {props.purchase.movieName}</FlexView>
            <FlexView>Visa kaina: {Math.round(props.purchase.totalPrice * 1000) / 1000}</FlexView>
        </div>
    );
};
export default PurchaseDetailsComponent;
const styles = {
    text: {
        fontSize: 12,
        fontWeight: 'bold'
    }
};
