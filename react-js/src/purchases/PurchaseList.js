import React from 'react';
import { List, Pagination } from 'semantic-ui-react';
import PurchaseDetailsComponent from './PurchaseDetailsComponent';
import FlexView from 'react-flexview/lib';

class PurchaseList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activePage: 1
        };
    }
    setActivePage = (e, { activePage }) => {
        this.setState({
            activePage: activePage
        });
    };
    showPurchases = () => {
        const start = (this.state.activePage - 1) * 10;
        const end = this.state.activePage * 10;
        return this.props.purchases.slice(start, end);
    };
    filterPurchases=(e)=>{
        this.setState({
            activePage:1
        });
        this.props.filterPurchases(e);
    }
    render() {
        return (
            <FlexView column={true}>            
                <input
                    type="text"
                    placeholder="Užsakymų paieška pagal pirkėją ir/arba pavadinimą"
                    onChange={(e)=>this.filterPurchases(e)}
                />
                <FlexView className="ui celled list" row="true">
                    <List divided animated>
                        {this.showPurchases().map(purchase => {
                            return (
                                <PurchaseDetailsComponent key={purchase.id} purchase={purchase} />
                            );
                        })}
                    </List>
                </FlexView>
                <Pagination
                    activePage={this.state.activePage}
                    boundaryRange={1}
                    size="mini"
                    totalPages={this.props.purchases.length / 10}
                    onPageChange={this.setActivePage}
                    siblingRange={2}
                />
            </FlexView>
        );
    }
}
export default PurchaseList;
