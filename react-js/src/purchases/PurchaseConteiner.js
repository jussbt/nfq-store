import React from 'react';
import PurchaseComponent from './PurchaseComponent';
import PurchaseList from './PurchaseList';
import axios from 'axios';
import { BASE_URL } from '../util/Constants';

class PurchaseConteiner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            purchases: [],
            searchValue: ''
        };
    }
    componentDidMount = () => {
        this.getAllPurchases();
    };
    filterPurchases = e => {
        this.setState({
            searchValue: e.target.value
        });
    };
    getAllPurchases = () => {
        axios
            .get(BASE_URL + '/purchases/movies')
            .then(response => {
                this.setState({
                    purchases: response.data
                });
            })
            .catch(error => {
                console.log(error);
            });
    };
    render() {
        return (
            <PurchaseComponent
                history={this.props.history}
                content={
                    <PurchaseList
                        purchases={this.state.purchases.filter(
                            purchase =>
                                purchase.buyer.includes(this.state.searchValue) ||
                                purchase.movieName.includes(this.state.searchValue)
                        )}
                        filterPurchases={this.filterPurchases}
                    />
                }
            />
        );
    }
}
export default PurchaseConteiner;
