import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePageContainer from './home/HomePageContainer';
import MoviesContainer from './movies/MoviesContainer';
import PurchaseConteiner from './purchases/PurchaseConteiner';

const App = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" render={props => <HomePageContainer {...props} />} />
            <Route path="/filmai" render={props => <MoviesContainer {...props} />} />
            <Route path="/pirkimai" render={props => <PurchaseConteiner {...props} />} />
        </Switch>
    </BrowserRouter>
);

export default App;
